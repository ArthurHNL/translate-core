# Configuring

The Fooxly Translations Core provides you with two options.
You can either configure the core to act as a singleton instance, which is sufficient in most cases.
There is also a possibility you want multiple instances for some reason, the Core also supports this.

## Singleton Instance

This configures the default export of the Core, because of this you can use the same import in every other file.

**Example File**: app.js

```js
import tsc from '@fooxly/translations-core'

tsc.configure({
  locale: 'en-US',
  fallback: 'en-US',
  envLocale: 'en-US',
  useFallback: true
})
```

## Multiple Instances

Export the instance in order for it to be accessible in other files.

**Example File**: translations/index.js

```js
import tsc from '@fooxly/translations-core'

export default tsc.configureInstance({
  locale: 'en-US',
  fallback: 'en-US',
  envLocale: 'en-US',
  useFallback: true
})
```
