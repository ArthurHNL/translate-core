# API

**Note**: This part of the documentation is incomplete

## Methods

### `configure: (options?: Options): void`

Configure the main instance of the Translations Core.

## Types

```ts
export interface Options {
  locale: string;         // Sets the locale to use (default='en-US')
  fallback: string;       // Sets the fallback locale to use if a translation for the
                          // current locale cannot be found (default=null)
  envLocale: string;      // Sets the environment locale to use if no locale is given (default=null')
  useFallback: boolean;   // Determines whether or not to use a fallback locale (default=false)
}
```
