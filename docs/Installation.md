# Installation

**Note**: You should not use this for normal applications, this package requires a wrapper.

## using npm

```sh
npm install --save @fooxly/translations-core
```

## using yarn

```sh
yarn add @fooxly/translations-core
```
