# Usage

When using a singleton instance, import like this:

```js
import { t } from '@fooxly/translations-core'
```

When using multiple instances, import like this:

(*assuming the instance is exported like the example in the [Configuring Instructions](Configuring.md)*)

```js
import { t } from '../translations'
```

## Set/Update Translations

**Example File**: app.js

```js
import { updateTranslations } from '@fooxly/translations-core'

updateTranslations({
  'en-US': {
    foo: {
      bar: 'Price: {{amount}}x ${{price}}'
    }
  }
})
```

## Set Locale

**Example File**: containers/settings.js

```js
import { setLocale } from '@fooxly/translations-core'

setLocale('en-US')
```

## Translate

**Example File**: component/text.js

```js
import { t } from '@fooxly/translations-core'
```

**Example Translation**:

```js
{
  foo: {
    bar: 'Price ...' // Translation Base
  }
}
```

### Object with values

**Translation Base**: `Price: {{amount}}x ${{price}}`

```js
t('foo.bar', { amount: 20, price: 10 })
// 'Price: 20x $10'
```

### Array with values

**Translation Base**: `Price: {{1}}x ${{0}}`

```js
t('foo.bar', [10, 20])
// Price: 20x $10
```

### Value

**Translation Base**: `Price: ${{value}}`

```js
t('foo.bar', 10)
// Price: $10
```

### No Value(s)

**Translation Base**: `Hello`

```js
t('foo.bar')
// Hello
```

### Using a default translation

If a translation can't be found for the current language,
the fallback language will be used unless a default translation is given.
If the default translation can't be found in the current language translations,
it'll be searched for in the fallback language translations.
If the default translation also can't be found in the fallback language translations,
the fallback language will be used for the initial requested translation.

**Translation Base**: `Hello`

```js
t('foo.barrr', null, 'foo.bar')
// Hello
```

Values can still be used for a default translation.

**Translation Base**: `Hello {{value}}`

```js
t('foo.barrr', null, 'foo.bar', 'World')
// Hello World
```
