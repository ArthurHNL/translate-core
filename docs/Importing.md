# Importing

## ES6 and up

The module uses an ES6 style export statement, simply use `import` to load the module.

```js
import tsc from '@fooxly/translations-core';
```

## ES5

If you're using an ES5 require statement to load the module, please add `default`. Look [here](https://stackoverflow.com/a/38459264/7448923) for more details.

**Note**: The remaining documentation is written in ES6.

```js
var tsc = require('@fooxly/translations-core').default;
```
