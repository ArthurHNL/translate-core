# Changelog

All notable changes to this project will be documented in this file.

The format is mostly based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

**Note**: We do not include unreleased changes.

## [1.1.1] - 2019-04-07
### Changed
- license & keywords field in package.json
- Moved external links in CHANGELOG.md to bottom
### Fixed
- Now redirects to GitLab for documentation
(useful when visiting package on [npmjs.com])

## [1.1.0] - 2019-04-07
### Added
- LICENSE file
- Test scripts and Git to package.json
- CI pipeline (thanks to [@ArthurHNL](https://gitlab.com/ArthurHNL))
- Badges to README
- Tests with 100% coverage
### Fixed
- Build process on Windows
### Changed
- Default values in code and docs to match tests
- .gitignore to support testing
- Formatted and improved .npmignore
- Minified build
- Started following [Keep a Changelog] partially,
and [Semantic Versioning] properly

## [1.0.2] - 2019-04-05
### Added
- .npmignore file for NPM packaging
### Fixed
- Build process on Windows
### Changed
- Seperated and improved documentation

## [1.0.1] - 2019-04-05
### Fixed
- NPM packaging

## [1.0.0] - 2019-04-05
### Added
- Initial release

<!-- Versions -->
[1.1.1]: https://gitlab.com/fooxly/translations/translations-core/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/fooxly/translations/translations-core/compare/v1.0.2...v1.1.0
[1.0.2]: https://gitlab.com/fooxly/translations/translations-core/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/fooxly/translations/translations-core/compare/v1.0.1...v1.0.0
[1.0.0]: https://gitlab.com/fooxly/translations/translations-core/tree/v1.0.0

<!-- External Links -->
[npmjs.com]: https://www.npmjs.com/package/@fooxly/translations-core
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
