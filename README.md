# Fooxly Translations Core

[![pipeline status](https://gitlab.com/fooxly/translate/translate-core/badges/master/pipeline.svg)](https://gitlab.com/fooxly/translate/translate-core/commits/master)
[![coverage report](https://gitlab.com/fooxly/translate/translate-core/badges/master/coverage.svg)](https://gitlab.com/fooxly/translate/translate-core/commits/master)
![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)
[![npm version](https://img.shields.io/npm/v/@fooxly/translations-core.svg?style=flat)](https://www.npmjs.com/package/@fooxly/translations-core)
![npm bundle size](https://img.shields.io/bundlephobia/min/@fooxly/translations-core.svg)

* [Installation](https://gitlab.com/fooxly/translations/translations-core/blob/master/docs/Installation.md)

* [Importing](https://gitlab.com/fooxly/translations/translations-core/blob/master/docs/Importing.md)

* [Configuring](https://gitlab.com/fooxly/translations/translations-core/blob/master/docs/Configuring.md)

* [Usage](https://gitlab.com/fooxly/translations/translations-core/blob/master/docs/Usage.md)

* [API](https://gitlab.com/fooxly/translations/translations-core/blob/master/docs/API.md)
