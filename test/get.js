import assert from 'assert'
import _get from '../src/get'

describe('Translations - Get from Object', () => {
  const translation = {
    foo: {
      bar: 'Hello World'
    }
  }

  it('should return tranlation', () => {
    const result = _get(translation, 'foo.bar')
    assert.strictEqual(result, 'Hello World')
  })

  it('invalid request should return null', () => {
    const result = _get(translation, [])
    assert.strictEqual(result, null)
  })

  it('empty request should return null', () => {
    const result = _get(translation)
    assert.strictEqual(result, null)
  })

  it('non-existing request should return null', () => {
    const result = _get(translation, 'foo.test')
    assert.strictEqual(result, null)
  })

  it('empty translations should return null', () => {
    const result = _get(null, 'foo.bar')
    assert.strictEqual(result, null)
  })
})
