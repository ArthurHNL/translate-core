import assert from 'assert'
import Ts from '../src/translations'

describe('Translations - Class', () => {
  let ts = new Ts()

  it('should return a Tranlations instance', () => {
    assert.strictEqual(typeof ts, 'object')
    assert.strictEqual(ts instanceof Ts, true)
  })

  describe('Values', () => {
    let ts = new Ts()

    it('locale   should have the default value', () => {
      assert.strictEqual(ts.locale, 'en-US')
    })

    it('fallback should have the default values', () => {
      assert.strictEqual(ts.fallback, null)
      assert.strictEqual(ts.useFallback, false)
    })
  })

  describe('Methods', () => {
    describe('setLocale()', () => {
      let ts = new Ts()

      it('should set the locale', () => {
        ts.setLocale('en-GB')
        assert.strictEqual(ts.locale, 'en-GB')
      })

      it('should not set the locale when empty', () => {
        ts.setLocale(null)
        assert.strictEqual(ts.locale, 'en-GB')
      })
    })

    describe('setFallbackLocale()', () => {
      it('should set the fallback locale', () => {
        let ts = new Ts()
        ts.setFallbackLocale('en-GB')
        assert.strictEqual(ts.fallback, 'en-GB')
        assert.strictEqual(ts.useFallback, true)
      })

      it('should not set    the fallback locale when empty', () => {
        let ts = new Ts()
        ts.setFallbackLocale(null)
        assert.strictEqual(ts.fallback, null)
        assert.strictEqual(ts.useFallback, false)
      })

      it('should not change the fallback locale when empty', () => {
        let ts = new Ts()
        ts.setFallbackLocale('en-GB')
        ts.setFallbackLocale(null)
        assert.strictEqual(ts.fallback, 'en-GB')
        assert.strictEqual(ts.useFallback, true)
      })
    })

    describe('updateTranslations()', () => {
      let ts = new Ts()
      const t = {
        'en-US': {
          foo: {
            bar: 'Hello World'
          }
        }
      }

      it('should update translations', () => {
        ts.updateTranslations(t)
        assert.deepStrictEqual(ts.translations, t)
      })

      it('should not update translations when empty', () => {
        ts.updateTranslations(null)
        assert.deepStrictEqual(ts.translations, t)
      })

      it('should not update translations when invalid', () => {
        ts.updateTranslations('')
        assert.deepStrictEqual(ts.translations, t)
      })
    })

    describe('t()', () => {
      let ts = new Ts()
      ts.updateTranslations({
        'en-US': {
          foo: {
            bar: 'Hello World'
          }
        }
      })

      it('should return empty string when empty', () => {
        const result = ts.t('')
        assert.strictEqual(result, '')
      })

      it('should return empty string when invalid', () => {
        const result = ts.t([])
        assert.strictEqual(result, '')
      })

      it('should return empty string when not found', () => {
        const result = ts.t('test')
        assert.strictEqual(result, '')
      })

      it('should return empty string when default not found', () => {
        const result = ts.t('test1', null, 'test2')
        assert.strictEqual(result, '')
      })

      describe('without Values', () => {
        it('should return translation', () => {
          const result = ts.t('foo.bar')
          assert.strictEqual(result, 'Hello World')
        })

        it('should return default translation', () => {
          const result = ts.t('foo.barrr', null, 'foo.bar')
          assert.strictEqual(result, 'Hello World')
        })

        describe('using fallback', () => {
          let ts = new Ts({
            locale: 'en-GB',
            useFallback: true
          })
          ts.updateTranslations({
            'en-US': {
              foo: {
                bar: 'Hello World'
              }
            }
          })

          it('should return translation', () => {
            const result = ts.t('foo.bar')
            assert.strictEqual(result, 'Hello World')
          })

          it('should return default translation', () => {
            const result = ts.t('foo.barrr', null, 'foo.bar')
            assert.strictEqual(result, 'Hello World')
          })
        })
      })

      describe('with Values', () => {
        let ts = new Ts()
        ts.updateTranslations({
          'en-US': {
            foo: {
              bar: 'Hello {{value}}'
            }
          }
        })

        it('should return default translation', () => {
          const result = ts.t('foo.barrr', null, 'foo.bar', 'World!')
          assert.strictEqual(result, 'Hello World!')
        })

        it('should return translation  when string', () => {
          const result = ts.t('foo.bar', 'World!')
          assert.strictEqual(result, 'Hello World!')
        })

        it('should return translation  when array', () => {
          ts.updateTranslations({
            'en-US': {
              foo: {
                bar: 'Hello {{1}}{{0}}'
              }
            }
          })
          const result = ts.t('foo.bar', ['!', 'World'])
          assert.strictEqual(result, 'Hello World!')
        })

        it('should return translation  when object', () => {
          ts.updateTranslations({
            'en-US': {
              foo: {
                bar: 'Hello {{test}}'
              }
            }
          })
          const result = ts.t('foo.bar', { test: 'World' })
          assert.strictEqual(result, 'Hello World')
        })

        it('should return empty string when boolean', () => {
          ts.updateTranslations({
            'en-US': {
              foo: {
                bar: 'Hello {{test}}'
              }
            }
          })
          const result = ts.t('foo.bar', true)
          assert.strictEqual(result, 'Hello {{test}}')
        })

        it('should return empty string when invalid object', () => {
          ts.updateTranslations({
            'en-US': {
              foo: {
                bar: 'Hello {{test}}'
              }
            }
          })

          const result1 = ts.t('foo.bar', Object.create({
            name: 'inherited',
            test: []
          }))
          assert.strictEqual(result1, 'Hello {{test}}')

          const result2 = ts.t('foo.bar', { test: [] })
          assert.strictEqual(result2, 'Hello {{test}}')
        })

        describe('using fallback', () => {
          let ts = new Ts({
            locale: 'en-GB',
            useFallback: true
          })
          ts.updateTranslations({
            'en-US': {
              foo: {
                bar: 'Hello {{value}}'
              }
            }
          })

          it('should return translation', () => {
            const result = ts.t('foo.bar', 'World!')
            assert.strictEqual(result, 'Hello World!')
          })

          it('should return default translation', () => {
            const result = ts.t('foo.barrr', null, 'foo.bar', 'World!')
            assert.strictEqual(result, 'Hello World!')
          })
        })
      })
    })
  })

  // TODO: confirm options after creating instance
  ts = new Ts({
    locale: 'en-US',
    fallback: 'en-GB',
    useFallback: false,
    envLocale: 'en-US'
  })

  ts = new Ts({
    fallback: 'en-GB',
    useFallback: true,
    envLocale: 'en-US'
  })

  ts = new Ts({
    useFallback: true,
    envLocale: 'en-US'
  })

  ts = new Ts({
    useFallback: true,
    envLocale: 'en-US'
  })
})
