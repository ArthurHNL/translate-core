import assert from 'assert'
import tsc from '../src'
import Ts from '../src/translations'

describe('Translations - Core', () => {
  // TODO: confirm options after configuring
  tsc.configure()
  tsc.configure({
    locale: 'en-GB'
  })

  describe('Methods', () => {
    describe('setLocale()', () => {
      it('should set the locale', () => {
        tsc.setLocale('en-GB')
        // TODO: get locale to confirm
        // assert.strictEqual(tsc.locale, 'en-GB')
      })
    })

    describe('setFallbackLocale()', () => {
      it('should set the fallback locale', () => {
        tsc.setFallbackLocale('en-GB')
        // TODO: get fallback locale to confirm
        // assert.strictEqual(tsc.locale, 'en-GB')
      })
    })

    describe('updateTranslations()', () => {
      it('should set the translations', () => {
        tsc.setLocale('en-US')
        tsc.updateTranslations({
          'en-US': {
            foo: {
              bar: 'Hello World'
            }
          }
        })

        const result = tsc.t('foo.bar')
        assert.strictEqual(result, 'Hello World')
      })
    })

    describe('configureInstance()', () => {
      it('should return a Translations instance', () => {
        let ts = tsc.configureInstance({
          locale: 'en-GB'
        })

        assert.strictEqual(typeof ts, 'object')
        assert.strictEqual(ts instanceof Ts, true)
      })

      it('should return a Translations instance when empty', () => {
        let ts = tsc.configureInstance()

        assert.strictEqual(typeof ts, 'object')
        assert.strictEqual(ts instanceof Ts, true)
      })
    })

    describe('t()', () => {
      it('t should get a translation', () => {
        tsc.setLocale('en-US')
        tsc.updateTranslations({
          'en-US': {
            foo: {
              bar: 'Hello World'
            }
          }
        })

        const result = tsc.t('foo.bar')
        assert.strictEqual(result, 'Hello World')
      })
    })
  })
})
