import Translations from './translations'

/** @type {Translations} */
var _translator = null

/**
 * Configures the Translations instance
 * @param     {Options} [options={}]
 *            Object with options for using Translations
 */
module.exports.configure = (options = {}) => {
  const tr = new Translations(options)
  _translator = tr
  return tr
}

/**
 * Configures a Translations instance
 * @param     {Options} [options={}]
 *            Object with options for using Translations
 */
module.exports.configureInstance = (options = {}) => {
  return new Translations(options)
}

module.exports.t =
  (...props) => _translator.t(...props)
module.exports.setLocale =
  (...props) => _translator.setLocale(...props)
module.exports.setFallbackLocale =
  (...props) => _translator.setFallbackLocale(...props)
module.exports.updateTranslations =
  (...props) => _translator.updateTranslations(...props)

/**
 * @typedef   {Object} Options
 * @property  {String} locale
 *            Sets the locale to use,
 *            if no locale is given FTS the default will be used
 *            (default='en-US')
 * @property  {String} fallback
 *            Sets the fallback locale to use
 *            if a translation for the current locale cannot be found
 *            (default=null)
 * @property  {String} envLocale
 *            Sets the environment locale which is used if no/incorrect locale is given
 *            (default=null)
 * @property  {Boolean} useFallback
 *            Determines whether or not to use a fallback locale
 *            (default=false)
 */
