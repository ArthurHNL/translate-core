export default function _get (object, path) {
  if (typeof path === 'string') {
    path = path.split('.')
  } else {
    return null
  }

  return path.reduce(function (xs, x) {
    return xs && xs[x] ? xs[x] : null
  }, object || {})
}
