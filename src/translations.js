import _get from './get'

export default class Translations {
  translations = {}

  locale = null
  fallback = null
  useFallback = false

  constructor (opts = {}) {
    const { locale, fallback, useFallback, envLocale } = opts

    if (locale && typeof locale === 'string') {
      this.locale = locale
    } else {
      this.locale =
        envLocale && typeof envLocale === 'string'
          ? envLocale
          : 'en-US'
    }

    if (useFallback) {
      this.useFallback = true
      if (fallback && typeof fallback === 'string') {
        this.fallback = fallback
      } else {
        this.fallback = 'en-US'
      }
    }
  }

  setLocale = l => {
    if (typeof l === 'string') {
      this.locale = l
    }
  }

  setFallbackLocale = l => {
    if (typeof l === 'string') {
      this.fallback = l
      this.useFallback = true
    }
  }

  updateTranslations = t => {
    if (typeof t === 'object' && !Array.isArray(t)) {
      this.translations = {
        ...this.translations, ...t
      }
    }
  }

  /**
   * @param     {String} [key=]
   *            Path to the key to look for in the translations seperated by dots (.)
   * @param     {Object} [values=]
   *            Object with values to insert in the found translation
   * @param     {Object} [def=]
   *            Default value to use if a translation can't be found
   * @param     {Object} [defValues=]
   *            Object with values to insert in the found default translation
   */
  t = (key, values, def, defValues) => {
    if (!key || typeof key !== 'string') {
      return ''
    }

    let result = _get(this.translations[this.locale], key)

    if (result === null) {
      if (def) {
        result = _get(this.translations[this.locale], def)

        if (result === null && this.useFallback && this.fallback) {
          result = _get(this.translations[this.fallback], def)
        }

        if (result !== null) {
          values = defValues
        }
      }

      if (result === null && this.useFallback && this.fallback) {
        result = _get(this.translations[this.fallback], key)
      }
    }

    if (result === null) {
      console.error('Translations Error:', `${key} does not exist`)
      return ''
    } else if (!values) {
      return result
    }

    if (typeof values === 'string') {
      result = result.replace('{{value}}', values)
    } else if (Array.isArray(values)) {
      values.forEach((v, i) => {
        result = result.replace(`{{${i}}}`, v)
      })
    } else if (typeof values === 'object') {
      for (const k in values) {
        if (values.hasOwnProperty(k)) {
          const newV = values[k]
          if (typeof newV === 'string') {
            result = result.replace(`{{${k}}}`, newV)
          }
        }
      }
    }

    return result
  }
}
